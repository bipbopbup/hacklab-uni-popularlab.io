+++
title = "Acerca de ..."
description = "Recopilación de herramientas y publicaciones sobre Programación, DevOps, Soberanía Tecnológica, Privacidad y Ciberseguridad"
date = "2020-09-26"
aliases = ["about-us", "about-hugo", "contact"]
author = "HackLab"
+++

En esta página web encontrarás la información del HackLab Uni Popular en La Ferroviaria