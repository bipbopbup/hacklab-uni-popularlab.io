+++
title = "Sobre"
description = "Recopilación de herramientas y publicaciones sobre Programación, DevOps, Soberanía Tecnológica, Privacidad y Ciberseguridad"
date = "2020-09-26"
aliases = ["sobre"]
author = "HackLab"
+++

Tradução em português. Apenas para demonstração, o resto do artigo não está traduzido.
