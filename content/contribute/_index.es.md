+++
title = "¿Cómo editar esta web?"
date = 2022-05-28T16:07:44+02:00
weight = 15
chapter = true
pre = "<b>1. </b>"
tags = [
    "Web estática",
    "Hugo"
]
toc = true
+++

### Capítulo 1

# ¿Cómo editar esta web?

En esta sección o capítulo se explica cómo contribuir o editar esta web estática generada con Hugo y desplegada con GitLab Pages. Existen diferentes maneras de editar la web, por lo que se exponen las diferentes formas en función de los conocimientos técnicos.
