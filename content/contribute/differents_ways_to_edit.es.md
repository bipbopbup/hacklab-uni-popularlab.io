+++
title = "Distintas formas de editar para editar esta web"
date = 2022-05-28T16:07:44+02:00
weight = 15
chapter = false
pre = "<b>1.1. </b>"
tags = [
    "Web estática",
    "Hugo",
    "GitLab"
]
toc = true
+++

- [**Editar desde la interfaz gráfica de una instancia de GitLab**](edit_from_Gitlab_UI.es.md). Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web.

    - Un contenido o entrada ya creada. 

    - Crear una entrada nueva en la web. No se necesitan conocimientos técnicos.

- **Editar con git**. Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos de git y quitarse el miedo a usar la terminal (aunque también existen interfaces gráficas para git :) ).

    - Un contenido o entrada ya creada.

    - Crear una entrada nueva en la web.

- **Editar desde el móvil con Termux y Markor**.  Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos de git y no tener miedo a una terminal. 