+++
title = "Editar desde la interfaz gráfica de una instancia de GitLab"
date = 2022-05-28T16:07:44+02:00
weight = 20
chapter = false
pre = "<b>1.2. </b>"
tags = [
    "Web estática",
    "Hugo",
    "GitLab"
]
toc = true
+++

Para seguir estos pasos, se requiere conocimientos básicos del lenguaje de marcado Markdown (muuuuuuy fácil de aprender) y una cuenta en la instancia de GitLab donde se aloja la página web.

### Editar un contenido o entrada ya creada

Pasos:

1. En la esquina superior derecha, existe un enlace con el nombre "Editar esta página" que dirige al GitLab donde esté alojado el código fuente de esta página web. Pinchar en el enlace "Editar esta página".

2. Iniciar sesión o registrarse en la instacia de GitLab en modo Standard con usuario y contraseña.

3. Modificar el contenido del fichero al que nos ha redirgido y pulsar "Commit changes". Para modificar el fichero, debemos seguir la sintaxis de Markdown. Existen muchos post en internet donde se explica la sintaxis de Markdown, uno de ellos es este [enlace](https://markdown.es/sintaxis-markdown/).

4. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.

### Crear una entrada nueva en la web

Pasos:

1. Iniciar sesión o registrarse en la instacia de GitLab en modo Standard con usuario y contraseña. Los enlaces se encuentran en la barra lateral en ["GitLab repo"](https://gitlab.com/hacklab-uni-popular/hacklab-uni-popular.gitlab.io) y ["Framagit repo"](https://framagit.org/hacklab/hacklab.frama.io).

2. Una vez nos encontremos en el repositorio de la página web, pincharemos en Web IDE para abrir un entorno de desarrollo integrado en GitLab. Podremos observar la estructura de directorios de un proyecto creado con Hugo. Si se quiere saber más sobre esta estructura, la mejor opción es ir directamente a la [documentación de Hugo sobre la estructura de directorios](https://gohugo.io/getting-started/directory-structure/).

3. Nos desplazaremos al archivo `content/contribute/template-to-edit.es.md` y copiaremos el código con la separación entre +++ (incluidas las separaciones).  

4. Una vez copiado, tendremos que decidir dónde crear la nueva entrada y si se trata de un capítulo nuevo o no. En caso de ser un nuevo capítulo, se requiere crear una nueva carpeta que contenga un archivo `_index.es.md` y en vez de copiar el archivo anterior, podremos copiar cualquier otro archivo `_index.es.md`. Una vez esté decido, crear un nuevo fichero con el nombre que se quiera y que termine en `.es.md` para que detecte el idioma en el que se encuentra escrito. 

5. Pegamos al inicio del fichero los metadatos de la plantilla modificando lo que sea necesario (título, fecha, peso, ...). Seguimos añadiendo el contenido que queramos. 

6. Una vez esté terminado (o queramos guardar los cambios para comprobar que todo funciona correctamente), pulsaremos **"Create commit"** y **"Commit to main branch"**. Escribimos un breve mensaje con los cambios realizados y pulsamos **"Commit"**.

6. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.