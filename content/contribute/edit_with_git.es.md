+++
title = "Editar con git"
date = 2022-05-28T16:07:44+02:00
weight = 25
chapter = false
pre = "<b>1.3. </b>"
tags = [
    "Web estática",
    "Hugo",
    "Git"
]
toc = true
+++

Para seguir estos pasos se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos de git y quitarse el miedo a usar la terminal (aunque también existen interfaces gráficas para git :) ).

### Editar un contenido o entrada ya creada

Pasos:

1. Clonar el repositorio.

2. Instalar Hugo, servir la web con `hugo serve` y abrir [http://localhost:1313]() en el navegador.

3. Abrir el proyecto con el editor de código preferido y modificar un archivo en la carpeta `content/`.

3. Visualizar los cambios en el navegador después de guardar el fichero modificado.

4. Hacer un commit en la rama main y subirlo a la instancia de GitLab.

4. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.

### Crear una entrada nueva en la web

Pasos:

1. Clonar el repositorio.

2. Instalar Hugo, servir la web con `hugo serve` y abrir [http://localhost:1313]() en el navegador.

3. Abrir el proyecto con el editor de código preferido y con una terminal. Si queremos crear un nuevo capítulo o sección con el nombre `nuevo`, ejecutamnos lo siguiente:

```zsh
hugo new --kind chapter nuevo/_index.md
```

Si simpleamente queremos crear una nueva entrada, escribimos:

```zsh
hugo new hugo/quick_start.md
```

3. Después, ya se podrá visualizar los cambios en el navegador después de guardar el fichero modificado.

4. Hacer un commit en la rama main y subirlo a la instancia de GitLab.

4. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.