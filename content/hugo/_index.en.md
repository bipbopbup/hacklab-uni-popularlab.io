+++
title = "Hugo"
date = 2022-06-20T12:17:37+01:00
weight = 20
chapter = true
pre = "<b>2. </b>"
tags = [
    "Web estática",
    "Hugo",
    "GitLab pages"
]
+++

### Chapter 2

# Hugo

First steps to create and deploy a Hugo project in GitLab pages.