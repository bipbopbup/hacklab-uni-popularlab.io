+++
title = "Generación de una página web con Hugo"
date = 2022-06-20T16:07:44+02:00
weight = 5
chapter = false
pre = "<b>2.1. </b>"
tags = [
    "Web estática",
    "Hugo",
    "Git"
]
toc = true
+++

### ¿Cómo crear una nueva página web con Hugo?

Pasos:

1. Instalamos Hugo siguiendo la [documentación oficial](https://gohugo.io/getting-started/installing/).

2. Elegimos un [tema](https://themes.gohugo.io/). Por ejemplo, el tema de esta web es [Learn](https://themes.gohugo.io/hugo-theme-learn/).

3. Creamos un nuevo proyecto con:

```
hugo new site <name-project>
```

4. Nos desplazamos a la carpeta e inicializamos el repositorio.

```
git init
```

5. Copiamos el archivo zip del tema elegido en la carpeta `themes` o añadimos el submódulo.

```
git clone https://github.com/matcornic/hugo-theme-learn themes/learn
```

O añadimos el submódulo:

```
git submodule add https://github.com/gesquive/slate themes/slate
```
6. Editamos el fichero de configuración `config.toml` y añadimos:

```
baseURL = "http://localhost:1313/"
theme = "learn"
```

7. Servimos la página web con el siguiente comando y está disponible en la ruta [http://localhost:1313/](http://localhost:1313/).

```
hugo server
```

8. Crear nuevo contenido en Hugo. Por ejemplo en el tema Learn, crearemos un nuevo capítulo con el siguiente comando:

```
hugo new --kind chapter hugo/_index.md
```

O si queremos crear una nueva entrada, escribimos:

```
hugo new hugo/quick_start.md
```

8. Publicamos el proyecto en GitLab.